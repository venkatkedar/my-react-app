FROM node:8
#RUN mkdir -p /usr/src/app
WORKDIR /app
COPY . .
RUN npm install
RUN npm build 
CMD  ["npm", "start"]