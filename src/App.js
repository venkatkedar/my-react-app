import React, { useState, Component } from 'react';

import logo from './logo.svg';
import './App.css';
import Window from './components/window/window.js'
import Rating from './components/rating/rating.js'
import Card from './components/card/card.js';
import {IconContext} from 'react-icons'
import {FaAnchor} from 'react-icons/fa'
import { Container,Row,Col, Navbar } from 'react-bootstrap';
import Jumbotron from './components/jumbo';
var moment=require('moment');
export default function App(){
  const [hover, setHover] = useState(false)  
   const flag=false
    return (
      <IconContext.Provider value={{className:'react-icons'}}>
        <Navbar bg="dark" expand="lg">
                  <Navbar.Brand href="#"style={{color:'white'}}>
                    React BootStrap 
                    <div style={{width:60,height:60,float:'left'}}>
                    <FaAnchor style={{margin:10}}size={hover?30:25} onMouseEnter={()=>setHover(true)} onMouseLeave={()=>setHover(false)} />                    
                    </div>
                  </Navbar.Brand>
                  <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
        </Navbar>
        <Jumbotron />
      </IconContext.Provider>
    );
  
}
