import React, { Component } from 'react';
import './window.css'

class Window extends Component{
      render(){
        return (
          <div className="window">
            <div className="window-header">
              {this.props.header}
            </div>
            <div className="window-body">
              {this.props.body_text}{this.props.header}
            </div>
          </div>
        );
    }
}

export default Window;
