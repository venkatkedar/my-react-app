import React from 'react'
import Rating from '../rating/rating.js'
function Card(props) {
    console.log(props);
    //debugger;
    let styles = { width: 300, height: 250, border: "1px solid black" }
    return (
        <div style={styles}>
            <img src={props.imgUrl} />
            <h3>{props.title}</h3>
            
            <Rating/>
            <p>{props.numViews} views</p>
            <p>{props.loc}</p>
        </div>


    );
}

export default Card
