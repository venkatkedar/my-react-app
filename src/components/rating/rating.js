import React from 'react'
import './rating.css'
import {MdStar} from 'react-icons/md';
//import {IconContext} from 'react-icons'
const Rating=(props)=>{
    var stars=[]
    for(var i=0;i<props.numStars;i++)
        stars.push(<li><MdStar size={props.size}/></li>)
    //var stars=list.map(a=>)
    return (
        <div className="rating">
            <ul>
                {stars}
            </ul>
        </div>
    );
}

Rating.defaultProps={
    numStars:5,
    size:30
}

export default Rating
